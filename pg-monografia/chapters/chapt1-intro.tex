%%% Section start %%%
\chapter{Introdução}
\label{chapt-intro}

Esse capítulo apresenta a introdução deste trabalho, que consiste do desenvolvimento de uma aplicação Web, com foco em uso de modelos para rastreabilidade de requisitos.

\section{Contexto e Motivação}
\label{sec-context}

O software desempenha um papel essencial na sociedade contemporânea e se tornou indispensável em muitos contextos de nossas vidas, papel este que motiva uma série de iniciativas de pesquisa destinadas a compreender a natureza do software e sua relação conosco. Uma concepção compartilhada nessas iniciativas é que o software é um artefato (social) complexo~\cite{wang-et-al:er2014,wang-et-al:fois2014,irmak:gps13}. Essa noção vem do fato de 
que um sistema de software moderno pode ser entendido como a combinação de uma série de elementos interagentes, especificamente organizados para fornecer um conjunto de funcionalidades para atender a propósitos humanos particulares~\cite{iso2010sevocab,bourque2014guide}.

Além disso, o software está em constante crescimento, não apenas em medidas simples como o número de linhas de código, mas também de acordo com outros fatores, como complexidade, criticidade e grau de heterogeneidade~\cite{cheng2007research}. Este crescimento se dá devido a vários fatores, que levam a mudanças nos requisitos. Uma das principais fontes geradoras de alteração é o próprio conjunto de interessados (\textit{stakeholders}), que inicialmente não tem muita clareza nos objetivos a alcançar, e a medida que o desenvolvimento evolui vai descobrindo novas possibilidades ou funcionalidades a incluir no projeto. Um adequado gerenciamento de requisitos necessita da rastreabilidade dos requisitos para controlar e documentar as modificações, além disso, a rastreabilidade ajuda a estimar variações em cronogramas e alterações nos custos de desenvolvimento.
Sem a rastreabilidade, por exemplo, a equipe de desenvolvimento poderia não saber a extensão das mudanças e, com isso, não saber valorar as mudanças requeridas.

Além de que o crescimento do software faz com que o software fique muito mais caro para se desenvolver e pode dar início a muitos problemas em seu ciclo de vida. Neste contexto, conceitos como problema, anomalia, bug e falha são geralmente tratados indistintamente, embora possam ter semânticas ontológicas diferentes. Esse uso informal, por mais comum e prático que seja em nossas conversas cotidianas, pode ser fonte de problemas de ambiguidade e falsa concordância, uma vez que o conceito de anomalia é frequentemente sobrecarregado, portanto, referindo-se a entidades com naturezas ontológicas distintas.
Em um ambiente mais formal, essa sobrecarga de construto pode levar a problemas e perdas de comunicação. Por isso é importante ter uma forma precisa de classificar os diferentes tipos de anomalias de software~\cite{duarte-et-al-ER2018OSDEF}.

O Guia de Conhecimento em Engenharia de Software (SWEBoK)~\cite{irmak:gps13} enfatiza, por exemplo, a necessidade de um consenso sobre a caracterização de anomalias e discute como uma classificação bem fundamentada pode ser usada em auditorias e análises de produtos. Além disso, o modelo CMMI~\cite{team2010cmmi} preconiza que as organizações devem criar ou reutilizar alguma forma de método de classificação de defeitos, erros e falhas. Também sugere o uso de um índice de densidade de defeito para muitos produtos de trabalho que fazem parte do processo de desenvolvimento de software. 

Um esquema de classificação adequado pode permitir o desenvolvimento de diferentes tipos de perfis de anomalias, que podem ser produzidos como um indicador da qualidade do produto. Além disso, classificar sistematicamente anomalias de software que podem ocorrer em tempo de design ou tempo de execução é uma rica fonte de dados, que pode ser usada para melhorar processos e evitar a ocorrência de anomalias em projetos futuros~\cite{IEEE-1044@2009}. Por fim, defeitos, erros e falhas têm um impacto negativo em aspectos importantes do software, como confiabilidade, eficiência, custo geral e, em última análise, vida útil. Portanto, uma melhor compreensão da natureza ontológica desses conceitos e como eles se relacionam com outros artefatos de software (por exemplo, requisitos, solicitações de mudança, relatórios e casos de teste) pode melhorar a forma como uma organização lida com esses problemas, reduzindo custos com atividades como gerenciamento de configuração e manutenção de software~\cite{duarte-et-al-ER2018OSDEF}.

Embora existissem algumas propostas para classificar diferentes termos para anomalias de software, não existia um modelo de referência ou teoria que explicasse a natureza das diferentes anomalias de software. Para suprir essa lacuna, foi proposta uma Ontologia de referência de Defeitos, Erros e Falhas de Software (OSDEF)~\cite{duarte-et-al2021DKE}, que será mais tarde descrita na Seção~\ref{sec-osdef}. Sabendo da importância de analisar tais anomalias, em termos do risco que a sua presença acarreta para os sistemas de software e, em particular, para estes sistemas como portadores de valor para algum agente, foi elaborada uma Ontologia de Referência em Sistemas de Software (ROSS)~\cite{duarte-et-al2021DKE}, sobre a relação entre sistemas e outros artefatos de software em diferentes níveis de requisitos, que pode se conectar a OSDEF quando se leva em consideração que uma falha a nível de programa afeta os objetivos da organização, podendo inclusive inviabilizar a existência do software. A ROSS também será descrita posteriormente na Seção~\ref{sec-ross}.

Essas ontologias são modelos, que servirão de referência para rastreabilidade de requisitos da aplicação utilizadas como exemplo, uma vez que na literatura propõe-se que para um funcionamento adequado de rastreabilidade é necessário que seja estabelecido um esquema formal de descrição das informações detalhadas, que facilite o raciocínio e que a aplicação de tecnologias semânticas promovam a utilização de metodologias e técnicas de rastreabilidade semântica, acarretando no gerenciamento de complexas relações entre itens de dados~\cite{roris-et-al2016towards}. Além disso, \citeonline{espinoza-gabarjosa2011study} sugerem que, para ter utilidade, a rastreabilidade necessariamente deve ser organizada em uma estrutura de modelagem adequada.

Como sugere o próprio significado, os chamados modelos de referência são \textit{templates} criados sobre uma demanda específica com a finalidade de reduzir significativamente a tarefa de criar outros novos modelos e/ou sistemas restritivos para aplicações. O funcionamento consiste na parte onde o usuário final do modelo de referência seleciona o que é ou não relevante do modelo e as adapta ao problema que
necessita da solução, configurando assim uma resposta a partir das partes adaptadas. Em outras palavras, modelos de referência podem ser compreendidos como uma abstração representativa de um conhecimento bem consolidado dentro de um domínio~\cite{ramesh-jarke2001toward}.



\section{Objetivos}
\label{intro-objectives}

O que o presente trabalho propõe é a criação de uma ferramenta gráfica baseada em tecnologia Java, na plataforma Web, que realize a rastreabilidade de requisitos utilizando como modelos de referência as ontologias ROSS e OSDEF~\cite{duarte-et-al2021DKE}, apresentando tal rastreabilidade de forma gráfica, para que seja visível de uma maneira mais amigável para o usuário final a relação entre requisitos.

Esse objetivo geral pode ser decomposto nos seguintes objetivos específicos.

\begin{itemize}
	
	\item Compreensão dos requisitos do sistema e das ontologias ROSS e OSDEF;
	
	\item Capacitação para usar a plataforma de desenvolvimento do Jakarta EE (antigo Java EE) e seus vários recursos;
	
	\item Desenvolvimento do sistema, incluindo documentação da arquitetura e projeto.
	
\end{itemize}


\section{Método de Pesquisa}
\label{intro-method}

Para atingir o objetivo geral apresentado na seção anterior, os seguintes passos
foram realizados:
\begin{enumerate}
	\item Revisão bibliográfica: leitura de materiais que forneçam uma revisão dos conceitos
	já estudados e materiais que apresentem conteúdos novos e necessários para o
	desenvolvimento do sistema;
	
	\item Análise do problema proposto: leitura dos trabalhos anteriores, com ênfase nos trabalhos realizados por~\citeonline{duarte-et-al2021DKE}, onde consta a descrição das ontologias em que este trabalho se baseia, e levantamento de requisitos para a aplicação;
	
	\item Estudo das tecnologias: leitura da documentação oficial da plataforma Jakarta EE, mas também de seus recursos como CDI, Maven e JSF. Além disso, uma pequena revisão sobre algumas tecnologias que seriam úteis ao projeto, a saber, SPARQL, Jena e PrimeFaces;
	
	\item Implementação do sistema: codificação da implementação do sistema utilizando as tecnologias definidas.
\end{enumerate}

\section{Organização}
\label{intro-organization}


O restante dessa monografia é organizado da seguinte forma:

O Capítulo~\ref{chapt-background} apresenta o estado da arte sobre Engenharia de Requisitos de software e a importância da rastreabilidade de requisitos.

O Capítulo~\ref{chapt-foundations} apresenta os fundamentos ontológicos adotados neste trabalho. Primeiramente, discutimos a importância e relevância das ontologias para este trabalho. Em segundo lugar, discutimos UFO~\cite{guizzardi:phdthesis05, guizzardi2007ontology, guizzardi-et-al:er13}, a ontologia de fundamentação adotada para a construção de ROSS e OSDEF. Finalmente apresentamos ROSS e OSDEF, as duas ontologias de referência de domínio adotadas como base para a realização desse trabalho.

O Capítulo~\ref{chapt-proposta} apresenta e discute a ferramenta que está sendo proposta com relação a rastreabilidade de requisitos.

O Capítulo~\ref{chapt-conclusions} apresenta os trabalhos relacionados, as conclusões desse trabalho, e as direções para trabalhos futuros.




