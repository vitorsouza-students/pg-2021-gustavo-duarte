%%% Section start %%%

\chapter{Engenharia de Requisitos e Rastreabilidade}
\label{chapt-background}

Este capítulo apresenta, na Seção~\ref{sec-background-reqeng}, um resumo sobre Engenharia de Requisitos de Software. Em seguida, a Seção~\ref{sec-background-rastreabilidade} foca no domínio rastreabilidade de requisitos de software e discute a importância da utilização de modelos de referência no domínio. Tais temas embasam a solução proposta neste trabalho.

\section{Engenharia de Requisitos de Software}
\label{sec-background-reqeng}

Sistemas de software são reconhecidamente importantes ativos estratégicos para diversas organizações. Com tal importância, é imprescindível que os sistemas funcionem de acordo com os requisitos estabelecidos. Sendo assim, um passo muito importante no desenvolvimento de software é a identificação e o entendimento dos requisitos dos negócios que os sistemas de software vão apoiar~\cite{aurum2005engineering}.

De acordo com \citeonline{pressman2016engenharia} o amplo espectro de tarefas e técnicas que levam a um entendimento dos requisitos é chamado de Engenharia de Requisitos, que se inicia durante a atividade de comunicação e continua na fase de modelagem, devendo ser adaptada às necessidades do processo, do projeto e das pessoas que realizam o trabalho.

Os requisitos podem ser definidos como as descrições das restrições operacionais e
dos serviços que devem ser providos pelo sistema~\cite{sommerville-2016-software-engineering}.
Nesse contexto, uma classificação que é amplamente aceita diz respeito ao tipo de informação documentada por um requisito. Requisitos funcionais são declarações de serviços
que o sistema deve prover, de como o sistema deve reagir a entradas específicas e de como o sistema deve se comportar em determinadas situações. Já os requisitos não-funcionais, são restrições aos serviços ou funções oferecidos pelo sistema, incluindo restrições no processo de desenvolvimento e restrições impostas pelas normas~\cite{sommerville-2016-software-engineering}.

Algo que pode ser útil na representação de requisitos é fazê-lo em níveis diferentes de descrição, pois os \textit{stakeholders}\footnote{\textit{Stakeholders} são as partes interessadas no desenvolvimento dos sistemas de software. Exemplos comuns de \textit{stakeholders} são os usuários finais do sistema e os diretores da organização que contrata a construção sistema de software.} são muito interessados em requisitos, mas possuem expectativas distintas. 
Dessa forma,~\citeonline{sommerville-2016-software-engineering} propõe a descrição de dois níveis de requisitos:

\begin{itemize}
	\item \textbf{Requisitos de usuário} são declarações, em uma linguagem natural com diagramas, de quais serviços o sistema deverá fornecer a seus usuários e as restrições com as quais este deve operar;

	\item \textbf{Requisitos de sistema} são descrições mais detalhadas das funções, serviços e restrições operacionais do sistema de software. O documento de requisitos do sistema (às vezes chamado especificação funcional) deve definir exatamente o que deve ser implementado. Pode ser parte do contrato entre o comprador do sistema e os desenvolvedores de software.
\end{itemize}

Mesmo que vários projetos requeiram processos com características específicas para contemplar suas peculiaridades, é possível estabelecer um conjunto de atividades básicas que deve ser considerado na definição de um processo de Engenharia de Requisitos. 
Tomando por base o processo proposto por~\citeonline{kotonya-sommerville1998requirements}, um processo de Engenharia de Requisitos padrão deve
contemplar as atividades de levantamento de requisitos, análise de requisitos, documentação de requisitos, verificação e validação de requisitos e gerência de requisitos, como apresentado na Figura~\ref{fig-eng-req}. 

\begin{figure}
	\centering
	\includegraphics[scale=1]{figures/fig-eng-req} 
	\caption{Processo de Engenharia de Requisitos~\cite{kotonya-sommerville1998requirements}}
	\label{fig-eng-req}
\end{figure}

\textbf{Levantamento de requisitos} é a fase inicial do processo de Engenharia de Requisitos e envolve as atividades de descoberta dos requisitos. Nessa fase, um esforço conjunto de clientes, usuários e especialistas de domínio é necessário, com o objetivo de entender a organização, seus processos, necessidades, deficiências dos sistemas de software atuais, possibilidades de melhorias, bem como restrições existentes~\cite{kotonya-sommerville1998requirements}.

Em seguida, inicia-se a fase de \textbf{análise de requisitos}, que serve como base da modelagem do sistema, usando como base os requisitos levantados.

Conforme discutido anteriormente, requisitos de usuário são descritos normalmente em linguagem natural, pois devem ser compreendidos por pessoas que não sejam especialistas técnicos. 
Entretanto, quanto mais detalhados os requisitos mais úteis eles serão para o desenvolvimento do sistema. Para isso, diversos tipos de modelos podem ser utilizados. Esses modelos são representações gráficas que descrevem processos de negócio, o problema a ser resolvido e o sistema a ser desenvolvido. Por utilizarem representações gráficas, modelos são geralmente mais compreensíveis do que descrições detalhadas em
linguagem natural~\cite{sommerville-2016-software-engineering}. 

Os modelos criados durante a atividade de análise de requisitos cumprem os papeis de: (i)~ajudar o analista a entender a informação, a função e o comportamento do sistema, tornando a tarefa de análise de requisitos mais fácil e sistemática; e (ii)~tornar-se o ponto focal para a revisão e, portanto, a chave para a determinação da consistência da especificação~\cite{pressman2009engenharia}.

Após a análise dos requisitos levantados, inicia-se a etapa de \textbf{documentação de requisitos} que é uma atividade de registro e oficialização dos resultados da Engenharia de Requisitos.

Uma boa documentação apresenta vários benefícios, alguns deles são: tornar fácil a comunicação dos requisitos; reduzir esforço de desenvolvimento, pois sua preparação força \textit{stakeholders} a considerar os requisitos atentamente, para evitar retrabalho posteriormente; prover uma base realista para estimativas, verificação e validação; facilitar a transferência do software para novos usuários e/ou máquinas, além de servir como base para futuras manutenções ou incremento de novas funcionalidades~\cite{nuseibeh2000requirements}. 

Quanto mais tarde defeitos e problemas são encontrados, maiores os custos associados à sua correção~\cite{rocha2001qualidade}, por isso uma etapa importante e que deve ser iniciada o mais breve possível é a etapa de \textbf{verificação e validação de requisitos}.
O objetivo da verificação é garantir que o software esteja sendo construído de forma correta, ou seja, deve-se verificar se os artefatos produzidos atendem aos requisitos estabelecidos e se os padrões organizacionais, de produto e processo, foram consistentemente aplicados.
Já a etapa de validação tem por objetivo assegurar que o software que está sendo desenvolvido é o software correto, que significa assegurar que os requisitos, e o software derivado deles, atendem ao uso proposto e às expectativas dos \textit{stakeholders}~\cite{rocha2001qualidade}.

Analisando os documentos de requisitos, as etapas de validação e verificação devem
garantir que um requisito, seja este uma regra de negócio, requisito funcional ou não funcional, deve ser~\cite{wiegers2013software, pfleeger2004engenharia}: completo, correto, consistente, realista, passível de ser priorizado, verificável e passível de confirmação e rastreável. Em outras palavras, devem garantir~\cite{pressman2009engenharia,kotonya-sommerville1998requirements,wiegers2013software}: a não ambiguidade de todos os requisitos, a detecção e correção de todas as inconsistências, conflitos, omissões e erros, a documentação em conformidade com os padrões estabelecidos e a real satisfação às necessidades dos clientes e usuários pelos requisitos.

Por fim, a \textbf{gerência de requisitos}, apresentada visualmente na Figura~\ref{fig-gerencia-requisitos}, é a atividade que atua na mudança dos requisitos. Tais mudanças ocorrem ao longo de todo o processo de software, desde o levantamento e análise de requisitos até depois do sistema entrar em operação. Essas mudanças decorrem de diversos fatores, dentre eles descoberta de erros, omissões, conflitos e inconsistências nos requisitos, melhor entendimento por parte dos usuários de suas necessidades, problemas técnicos, de cronograma ou de custo, mudança nas prioridades do cliente, mudanças no negócio, aparecimento de novos competidores, mudanças econômicas, mudanças na equipe, mudanças no ambiente onde o software será instalado
e mudanças organizacionais ou legais. Para minimizar as dificuldades impostas por essas mudanças, é necessário gerenciar requisitos~\cite{togneri2002apoio}.

A gerência de requisitos são atividades que ajudam a equipe de desenvolvimento a identificar, controlar e rastrear requisitos ao longo do ciclo de vida do software~\cite{kotonya-sommerville1998requirements,pressman2009engenharia}. Os principais objetivos desse processo de acordo com~\citeonline{kotonya-sommerville1998requirements} são a gerência de alteração de requisitos, a gerência de relacionamento de requisitos e a gerência de dependências entre requisitos e outros documentos produzidos no processo de software.
Para isso o processo de gerência de requisitos deve incluir as atividades de controle de mudanças, controle de versão, controle de estado dos requisitos e rastreamento de requisitos~\cite{wiegers2013software}.

\begin{figure}
	\centering
	\includegraphics[scale=0.9]{figures/fig-gerencia-requisitos} 
	\caption{Atividades de Gerência de Requisitos~\cite{wiegers2013software}}
	\label{fig-gerencia-requisitos}
\end{figure}

O controle de mudança define os procedimentos, processos e padrões que devem ser utilizados para gerenciar as alterações de requisitos, garantindo que toda proposta de mudança seja analisada conforme os critérios estabelecidos pela organização~\cite{kotonya-sommerville1998requirements}. 
O controle de mudanças, em geral, envolve atividades como~\cite{kotonya-sommerville1998requirements, wiegers2013software}: verificar a validade da mudança, rastrear informações sobre que requisitos e artefatos são afetados pela mudança, estimar o custo e impacto da mudança, negociar as mudanças com os cliente e alterar os requisitos e documentos associados aos mesmos.

Ao se detectar a necessidade de mudança em um ou mais requisitos, uma solicitação deve ser registrada e passar por uma avaliação da equipe de projeto. Avaliação esta, que deverá verificar o impacto, que é uma parte crítica do controle de mudanças, e os valores de custo, esforço, tempo e viabilidade deverão ser passados para o solicitante da mudança.
Além disso, para realizar essa mudança é necessário estabelecer uma rede de ligações de modo que um requisito e os elementos ligados a ele possam ser rastreados, identificado unicamente cada requisito e garantindo a possibilidade de saber quais requisitos dependem de que outros requisitos~\cite{deengenharia}.
Surge então o conceito de \textbf{rastreabilidade}, que será abordado na próxima seção.


\section{Rastreabilidade de Requisitos}
\label{sec-background-rastreabilidade}

A rastreabilidade pode ser definida como um grau relacional estabelecido entre duas ou mais entidades lógicas, especificamente as que possuem um predecessor-sucessor ou mestre-subordinado entre elas. 
Segundo~\citeonline{gotel-finkelstein1994traceabilityproblem}, rastreabilidade de requisitos tem como definição a capacidade de seguir o ciclo de vida de um requisito, seja este requisitos de mesmo nível seja entre diferentes diferentes níveis no processo de Engenharia de Software. 

Neste trabalho usamos como exemplo os requisitos de um software de caixa eletrônico elicitados por \citeonline{bjork2009ATM}, apresentados no Apêndice~\ref{chapt-apendice}. Como um exemplo de rastreabilidade, podemos identificar que o requisito de \textit{stakeholder} \textsf{STREQ001} --- ``O caixa eletrônico se comunicará com o computador do banco por meio de um link de comunicação apropriado'' --- impacta  diretamente o requisitos de sistema \textsf{SYSREQ001} --- ``O caixa eletrônico deve ter uma conexão de Internet adequada para se comunicar com o sistema do banco'' ---, ao mesmo tempo que o \textsf{SYSREQ001} é também impactado por \textsf{STREQ001}, como pode ser visto na Tabela~\ref{table-system-requirements} no Apêndice~\ref{chapt-apendice}. Tal exemplo ilustra a rastreabilidade bidirecional dos requisitos.

Há quase 25 anos a rastreabilidade de requisitos vem sendo reconhecida nas principais literaturas científicas como uma atividade capaz de melhorar a qualidade de gestão de sistemas de softwares~\cite{gotel-finkelstein1994traceabilityproblem,ramesh1995implementing,ramesh1997requirements}. Em outras palavras, a capacidade de rastrear de requisitos permite as partes interessadas acompanhar evolução do sistema e medir o impacto das mudanças que acontecem ao longo do ciclo de vida do software.
Além disso, modelos de maturidade~\cite{team2010cmmi, bourque2014guide} e padrões internacionais~\cite{iso2017traceability,iso29148reqeng} sugerem que a aptidão de rastrear requisitos mutáveis e relacioná-los a outros artefatos no ciclo de vida do software é indispensável para produtos de software de alta qualidade.

Requisitos são um artefato de alta prioridade e a fundação de todos os projetos de software; porém, muitos autores apontam a importância e relevância de outros muitos artefatos de software para o ciclo de vida do software. Esses artefatos estão diretamente relacionados aos requisitos de software por relações de dependência genérica ou mesmo existencial.\footnote{Por exemplo, um caso de teste pode ser \textit{existencialmente dependente} de um requisito funcional.} Tais relações, embora muitas vezes negligenciadas durante o ciclo de vida software, apresentam uma importante fonte de conhecimento sobre o domínio do sistema de software e facilitam sua gestão. Nesse contexto, uma das classificações mais aceitas na literatura propõe a separação entre rastreabilidade de requisitos horizontais e verticais~\cite{lindvall1996practical}.\footnote{Um exemplo comum de rastreabilidade de requisitos com outros artefatos dentro do ciclo de vida do software acontece na relação entre os casos de teste e os requisitos que são submetidos aos testes.}

A rastreabilidade de requisitos horizontal está ligada à atividade de rastrear as relações existentes entre os elementos em diferentes modelos.\footnote{Por exemplo, um programa PROG002 pode ser rastreado até um requisito de programa PROGREQ002 em um projeto de software}
Já a rastreabilidade vertical se refere ao rastreio das relações entre os elementos no mesmo modelo.\footnote{Por exemplo, o PROGREQ002 usado no exemplo anterior também pode estar diretamente relacionado a outros requisitos de programa, como realmente o PROGREQ002 é relacionado ao PROGREQ014, como pode ser visto na tabela~\ref*{table-ATM-programs} no apêndice~\ref{chapt-apendice}}


\begin{figure}
	\centering
	\includegraphics[scale=0.5]{figures/fig-background-requirements-traceability-overview} 
	\caption{Visão Geral de Rastreabilidade de Requisitos~\cite{kannenberg2009whytraceability}}
	\label{fig-background-requirements-traceability-overview}
\end{figure}

A Figura~\ref{fig-background-requirements-traceability-overview} apresenta uma visão geral do domínio de rastreabilidade de requisitos e descreve os conceitos de rastreabilidade horizontal e vertical que foram discutidos acima.

No entanto, construir e manter um mecanismo eficiente que forneça rastreabilidade entre requisitos é uma tarefa de alta complexidade. Diversos pesquisadores~\cite{gotel-finkelstein1994traceabilityproblem,kannenberg2009whytraceability, regan2012barriers,tufail2017traceability-slr} apresentaram ao longo dos anos, estudos que citam os maiores e mais comuns problemas para estabelecer uma política de rastreabilidade de requisitos eficaz. Problemas como decaimento da rastreabilidade, complexidade dos sistemas, pouco suporte de ferramentas, comunicação ineficaz entre as partes interessadas, integração deficiente de dados e até mesmo a limitação na geração de dados necessários, são listados como principais obstáculos a serem superados no campo de estudo e aplicabilidade. 

Dependendo do nível de maturidade da organização, os links de rastreabilidade são guardados de forma física em planilhas ou documentos baseados em texto, onde os links tendem a se deteriorar durante um projeto à medida que as atualizações, que são responsabilidades dos membros da equipe equilibrando-se em diversas outras tarefas operacionais, por vezes comprometem a atualização dos dados necessários para uma resposta eficaz na rastreabilidade dos dados.

Além disso, embora a rastreabilidade de requisitos seja constantemente referenciada em diversos padrões internacionais e modelos de maturidade, os requisitos específicos do processo ou diretrizes sobre como ela deve ser implementada raramente são  fornecidos~\cite{kirova2008effective}. Esse fenômeno acontece pois é sugerido que, como cada organização possui peculiaridades em seus processos, determinados fluxos de trabalho, 
produtos, recursos e serviços, elas necessitam da criação ou adaptação de um método viável e acessível para as demandas da organização.

Uma potencial solução para esses problemas é a utilização de ferramentas de gerenciamento de requisitos, como por exemplo, o IBM Rational RequisitePro, no entanto, tanto essa como outras ferramentas, em questão, tem frequentemente um custo muito elevado e muitas vezes acabam não sendo uma opção acessível para organizações de pequeno e médio porte. Por isso, infelizmente, muitas das organizações não priorizam a implementação das práticas de rastreabilidade, seja pelas dificuldades mencionadas ou porque sucumbem ao equívoco de que as práticas de rastreabilidade possuem apenas resultados de médio ou longo prazo para as organizações\cite{cleland2007bestpracticestraceability}. 

\citeonline{roris-et-al2016towards} propõem que para um funcionamento adequado de rastreabilidade é necessário que seja estabelecido um esquema formal de descrição das informações detalhadas que facilite o raciocínio e que a aplicação de tecnologias semânticas promovam a utilização de metodologias e técnicas de rastreabilidade semântica, acarretando no gerenciamento de complexas relações entre itens de dados.
Segundo também mencionado por~\citeonline{espinoza-gabarjosa2011study}, o uso da semântica como questão fundamental no que diz respeito à rastreabilidade é imprescindível. 
~\citeonline{ramesh-jarke2001toward} sugerem que, para ter utilidade, a rastreabilidade necessariamente deve ser organizada em uma estrutura de modelagem adequada. 

Devido a essas características e obstáculos presentes no domínio, diversos pesquisadores têm empenhado esforços com relação ao  desenvolvimento de modelos que servem de referência para rastreabilidade de requisitos. Possibilitando enxergar uma janela para a realidade do que futuramente pode ser adotado em organizações com base em suas demandas individuais. 
Como sugere o próprio significado, os chamados modelos de referência são \textit{templates} criados sobre uma demanda específica, com a finalidade de reduzir significativamente a tarefa de criar outros novos modelos e/ou sistemas restritivos para aplicações. O funcionamento consiste na parte onde o usuário final do modelo de referência seleciona o que é ou não relevante do modelo e as adapta ao problema que necessita da solução configurando assim uma resposta a partir das partes adaptadas. Em outras palavras, modelos de referência podem ser compreendidos como uma abstração representativa de um conhecimento bem consolidado dentro de um domínio~\cite{ramesh-jarke2001toward}.\footnote{De forma geral, domínios com elevado nível de padronização, como o domínio dos sistemas de software contemporâneos, podem ser beneficiados pela utilização de modelos de referência.}


O trabalho de~\citeonline{ramesh-jarke2001toward} 
é considerado um dos mais importantes e precursores do objeto de pesquisa referente à rastreabilidade que se baseia em modelos de referência. Eles propõem que a eficiência e a eficácia da rastreabilidade dependem fortemente do sistema de objetos e tipos de links de rastreabilidade que são utilizados. Além disso, os autores também indicam  que os modelos referenciados podem ser desenvolvidos baseados nos diferentes aspectos do campo de rastreabilidade de requisitos. Por exemplo, o modelo de referência deles para rastreabilidade de requisitos está focado nos Objetos (Artefatos) que existem no processo de software, enquanto o Modelo Semântico proposto por \citeonline{roris-et-al2016towards} está focado 
na caracterização do domínio do processo de rastreabilidade de tipos de rastros. 
Esse e muitos outros estudos podem ser  facilmente encontrados na literatura com diferentes abordagens para o desenvolvimento de um mecanismo de rastreabilidade que, com base em modelos, tem o objetivo primário de fornecer resultados palpáveis e eficientes, além de suporte geral do processo para os usuários finais do recurso.
 
A abordagem proposta por~\citeonline{zhang2014investigating} da ênfase nas dependências existentes entre os requisitos. Eles utilizam repetidamente o modelo de dependência de requisitos de Pohl e o modelo de dependência de requisitos (inter) de~\cite{dahlstedt2005requirements} para dessa forma elaborar um novo e utilizar como modelo de referência para uma rastreabilidade de requisitos em um estudo de caso de base industrial. 

Por fim, a utilização de modelos de referência é frequentemente apontada dentro da literatura de rastreabilidade de requisitos como meio de representar os aspectos que mais importam no processo de rastreabilidade para uma organização. Conforme citado anteriormente nessa seção, a rastreabilidade de requisitos pode fornecer inúmeros benefícios para o processo de software de uma organização, porém, o processo tende a se tornar mais complexo e mais suscetível a erros conforme o software naturalmente avança por seu ciclo de vida e sofre alterações.
A utilização de modelos de referência é uma tentativa de organizar algumas das opções que podem ser realizadas, no sentido de que a organização necessita seguir diretrizes propostas no modelo, ao mesmo passo traz facilidade no entendimento dos \textit{stakeholders} do processo de software.



